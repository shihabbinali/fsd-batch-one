var express = require('express');
var app = express();
var db = require("mysql");

var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(__dirname + 'public')); //Serves resources from public folder

//Serves all the request which includes /images in the url from Images folder
app.use('/images', express.static(__dirname + '/Images'));

var con = db.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "full_stack"
});



app.get('/', function (req, res) {

    res.sendFile(__dirname+'/index.html');
});

app.get('/submit-data', function (req, res) {
    res.send('Get submit Request');
});

app.post('/submit-data', function (req, res) {

con.connect(function(err) {
  //if (err) throw err;
  var sql = "INSERT into students (id,studentName, studentRollNumber) values ('100','"+req.body.username+"','"+req.body.roll+"')";
  console.log(sql);

	con.query(sql, function (err, result, fields) {
    //if (err) throw err;
    console.log(result);
  });
});


    res.send('POST Request :'+req.body.username+" with password "+req.body.password);
});

app.put('/update-data', function (req, res) {
    res.send('PUT Request');
});

app.delete('/delete-data', function (req, res) {
    res.send('DELETE Request');
});

var server = app.listen(5000, function () {
    console.log('Node server is running..');
});